package main

import (
        "encoding/json"
        "gorm.io/driver/postgres"
        "gorm.io/gorm"
        "log"
        "net/http"
)

var (
        db  *gorm.DB
        err error
)

type ErrorMessage struct {
        Error string
}

func handler(w http.ResponseWriter, r *http.Request) {
        var records []ServiceStatus
        answer := db.Find(&records)
        if answer.Error != nil {
                _ = json.NewEncoder(w).Encode(ErrorMessage{Error: "Database is unavailable"})
        } else {
                _ = json.NewEncoder(w).Encode(Response{Ip: "10.0.0.14", Services: records})
        }
}

type Response struct {
        Ip       string          `json:"ip"`
        Services []ServiceStatus `json:"services"`
}

type ServiceStatus struct {
        gorm.Model `json:"-"`
        Ip         string `gorm:"unique" json:"ip"`
        Status     string `json:"status"`
}

func main() {
        db, err = gorm.Open(postgres.Open("host=dbhost user=postgres password=password"), &gorm.Config{})
        if err != nil {
                panic("failed to connect database")
        }

        err = db.AutoMigrate(&ServiceStatus{})
        if err != nil {
                panic("failed to migrate database")
        }

        answer := db.Create(&ServiceStatus{Ip: "10.0.0.14", Status: "AVAILABLE"})
        if answer.Error != nil {
                log.Println(answer.Error)
        }

        http.HandleFunc("/healthcheck", handler)
        log.Fatal(http.ListenAndServe(":80", nil))
}